# Lib file produced by cargo
rust_output_lib=target/release/libaffine_arith.a

OUTPUT_HPP=affine_arith.hpp
OUTPUT_CPP=affine_arith.cpp

# The name of the static lib containing C++ functions
CPP_LIB=build/libaffine_arith_cpp.a
RUST_LIB=build/libaffine_arith_rust.a

CPP_O=$(patsubst build/%.a, build/%.o, ${CPP_LIB})


build: cargo ${OUTPUT_HPP} ${OUTPUT_CPP} ${RUST_LIB} ${CPP_LIB}

${RUST_LIB}: ${rust_output_lib} Makefile | cargo
	@mkdir -p ${@D}
	@echo -e "[\033[0;34mcp\033[0m] Copying $@"
	@cp ${rust_output_lib} $@

${CPP_LIB}: ${CPP_O} Makefile | cargo
	@mkdir -p ${@D}
	@echo -e "[\033[0;34mar\033[0m] Archiving $@"
	@ar rcs $@ $<

${OUTPUT_HPP}: src/ffi.rs Makefile | cargo
	@mkdir -p ${@D}
	@echo -e "[\033[0;34mcxxbridge\033[0m] Building $@"
	@cxxbridge src/ffi.rs --header > $@

${OUTPUT_CPP}: src/ffi.rs Makefile | cargo
	@mkdir -p ${@D}
	@echo -e "[\033[0;34mcxxbridge\033[0m] Building $@"
	@cxxbridge src/ffi.rs > $@

${CPP_O}: ${OUTPUT_CPP} Makefile | cargo
	@mkdir -p ${@D}
	@echo -e "[\033[0;34mclang++\033[0m] Building $@"
	@clang++ $< -c -o $@ -fPIC

cargo: .PHONY
	@cargo build --release --no-default-features

${rust_output_lib}: cargo

clean:
	cargo clean
	rm -rf ${OUTPUT_CPP} ${OUTPUT_HPP} ${RUST_LIB} build

.PHONY:
