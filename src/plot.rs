use gnuplot::{Figure, Caption, Color, AxesCommon, Axes2D};

use crate::Aa;
use crate::JoiningIterator;

use std::cmp::Ordering;

pub fn draw_polytope_on_axes(axes: &mut Axes2D, x: &Aa, y: &Aa) {
    let fcmp = |(x, _): (usize, f64), (y, _): (usize, f64)| x.cmp(&y);

    // Create a list of eror variable pairs 
    let mut x_ = x.errors.iter().peekable();
    let mut y_ = y.errors.iter().peekable();

    let mut combined = vec![];

    while x_.peek().is_some() && y_.peek().is_some() {
        let cmp = fcmp(**x_.peek().unwrap(), **y_.peek().unwrap());
        if cmp == Ordering::Less {
            combined.push(x_.next().map(|(_, x)| (*x, 0.)).unwrap())
        }
        else if cmp == Ordering::Greater {
            combined.push(y_.next().map(|(_, y)| (0., *y)).unwrap())
        }
        else {
            combined.push((x_.next().unwrap().1, y_.next().unwrap().1));
        }
    }
    combined.append(&mut x_.map(|(_, x)| (*x, 0.)).collect());
    combined.append(&mut y_.map(|(_, y)| (0., *y)).collect());

    // Generate corners of the lines corresponding to each error variable
    let mut lines = vec![];
    for bitmask in 0..(combined.len().pow(2)) {
        for i in 0..combined.len() {
            println!("combined: {:?}", combined[i]);
            let corner_fn = |match_coeff: f64| {
                combined.iter().enumerate()
                    .map(|(n, (x, y))| {
                        // If this is the coefficient we are flipping to get the point
                        let coeff = if n == i {
                            match_coeff
                        } else {
                            // Else, check if this should be inverted
                            if bitmask >> n & 0b1 == 0 {
                                -1.
                            }
                            else {
                                1.
                            }
                        };
                        (x * coeff, y*coeff)
                    })
                    .fold((x.center, y.center), |(x1, y1), (x2, y2)| (x1 + x2, y1 + y2))
            };

            // Generate the corners of the first line
            let point1 = corner_fn(1.);
            let point2 = corner_fn(-1.);

            lines.push((point1, point2));
        }
    }

    // axes.set_x_range(0., 20.);
    for (point1, point2) in lines {
        axes
            .lines(&[point1.0, point2.0], &[point1.1, point2.1], &[Color("red")]);
    }
}

pub fn draw_aa_polytope_with_function(
    x: &Aa,
    y: &Aa,
    extra_lines: (f64, f64, impl Fn(f64) -> f64)
) {
    let mut fig = Figure::new();
    let mut axes = fig.axes2d();

    draw_polytope_on_axes(&mut axes, x, y);

    let (min, max, fun) = extra_lines;
    let x = (0..1000)
        .map(|x| min + (x as f64 / 1000.) * max-min)
        .collect::<Vec<_>>();

    let y = x.iter().cloned().map(fun).collect::<Vec<_>>();

    axes.lines(&x, &y, &[Color("blue")]);

    fig.show()
        .expect("failed to show figure")
        .wait()
        .expect("failed to wait for figure to close");

}
pub fn draw_aa_polytope(
    x: &Aa,
    y: &Aa,
) {
    let mut fig = Figure::new();
    let mut axes = fig.axes2d();

    draw_polytope_on_axes(&mut axes, x, y);

    fig.show()
        .expect("failed to show figure")
        .wait()
        .expect("failed to wait for figure to close");

}


