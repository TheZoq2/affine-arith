use affine_arith::{Symbols, Aa, plot::draw_aa_polytope_with_function};

fn main() {
    let mut symbols = Symbols::new();
    let input = Aa::from_bounds(0.5, 2., &mut symbols);

    let result = input.sqrt(&mut symbols);

    draw_aa_polytope_with_function(&input, &result, (0., 2.5, |x: f64| x.sqrt()));
}
