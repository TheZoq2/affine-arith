use std::cmp::Ordering;

pub struct JoiningIterator<I1, I2, F1, F2, FJoin, FCmp>
{
    i1: I1,
    i2: I2,
    f1: F1,
    f2: F2,
    fjoin: FJoin,
    fcmp: FCmp,
}

impl<T, I1, I2, F1, F2, FJoin, FCmp> JoiningIterator<I1, I2, F1, F2, FJoin, FCmp>
    where I1: Iterator<Item = T>,
          I2: Iterator<Item = T>,
          F1: Fn(T) -> T,
          F2: Fn(T) -> T,
          FJoin: Fn(&T, &T) -> T,
          FCmp: Fn(&T, &T) -> Ordering
{
    pub fn new(i1: I1, i2: I2, f1: F1, f2: F2, fjoin: FJoin, fcmp: FCmp) -> Self {
        Self {
            i1,
            i2,
            f1,
            f2,
            fjoin,
            fcmp,
        }
    }
}

impl<T, I1, I2, F1, F2, FJoin, FCmp> Iterator for JoiningIterator<I1, I2, F1, F2, FJoin, FCmp>
    where I1: Iterator<Item = T> + Clone,
          I2: Iterator<Item = T> + Clone,
          F1: Fn(T) -> T,
          F2: Fn(T) -> T,
          FJoin: Fn(&T, &T) -> T,
          FCmp: Fn(&T, &T) -> Ordering
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        // None
        let mut peek_i1 = self.i1.clone().peekable();
        let mut peek_i2 = self.i2.clone().peekable();

        if peek_i1.peek().is_some() && peek_i2.peek().is_some() {
            let cmp = (self.fcmp)(peek_i1.peek().unwrap(), peek_i2.peek().unwrap());
            if cmp == Ordering::Less {
                self.i1.next().map(|x| (self.f1)(x))
            }
            else if cmp == Ordering::Greater {
                self.i2.next().map(|x| (self.f2)(x))
            }
            else {
                Some((self.fjoin)(
                    &self.i1.next().map(|x| (self.f1)(x)).unwrap(),
                    &self.i2.next().map(|x| (self.f2)(x)).unwrap()
                ))
            }
        }
        else {
            self.i1.next().map(|x| (self.f1)(x))
                .or(self.i2.next().map(|x| (self.f2)(x)))
        }
    }
}
