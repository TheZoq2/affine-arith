use affine_arith::{Symbols, Aa, plot::draw_aa_polytope};

fn main() {
    let x = Aa {
        center: 10.,
        errors: vec![(0, 2.), (1, 1.), (3, -1.)]
    };
    let y = Aa {
        center: 20.,
        errors: vec![(0, -3.), (2, 1.), (3, 4.)]
    };


    draw_aa_polytope(&x, &y)
}
