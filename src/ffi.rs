use std::sync::{Arc, Mutex};
use crate::{Ia, Aa};

// pub type ArcAa = Arc<Aa>;
pub struct ArcAa (Arc<Aa>);

impl ArcAa {
    pub fn new(inner: Aa) -> Self {
        Self (Arc::new(inner))
    }
}

impl std::ops::Deref for ArcAa {
    type Target = Arc<Aa>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Safe wrapper around the symbol table
pub struct Symbols {
    inner: Arc<Mutex<crate::Symbols>>
}


/// Create a new symbol tracker
pub fn new_symbols() -> Box<Symbols> {
    Box::new(Symbols{inner: Arc::new(Mutex::new(crate::Symbols::new()))})
}

/// Create an affine arithmetic struct with the specified bounds
#[no_mangle]
pub fn from_bounds(min: f64, max: f64, symbols: &Symbols) -> Box<Aa> {
    Box::new(Aa::from_bounds(min, max, &mut symbols.inner.lock().unwrap()))
}

/// Converts an AA struct into bounds
pub fn to_bounds(x: &Aa) -> ffi::Bounds {
    let Ia(min, max) = x.to_ia_range();
    ffi::Bounds{min, max}
}

fn clone_aa(val: &Box<Aa>) -> Box<Aa> {
    val.clone()
}


pub fn constant(value: f64) -> Box<Aa> {
    Box::new(Aa::constant(value))
}
pub fn add(lhs: &Aa, rhs: &Aa) -> Box<Aa> {
    Box::new(lhs + rhs)
}
pub fn sub(lhs: &Aa, rhs: &Aa) -> Box<Aa> {
    Box::new(lhs - rhs)
}
pub fn neg(x: &Aa) -> Box<Aa> {
    Box::new(-x)
}

#[cxx::bridge(namespace = aa)]
mod ffi {
    /// Struct representing bounds on a value. Generally produced as output from
    /// AA
    pub struct Bounds {
        pub min: f64,
        pub max: f64,
    }

    extern "Rust" {
        type Aa;
        type Symbols;
        fn new_symbols() -> Box<Symbols>;

        fn from_bounds(min: f64, max: f64, symbols: &Symbols) -> Box<Aa>;
        fn to_bounds(x: &Aa) -> Bounds;
        fn add(lhs: &Aa, rhs: &Aa) -> Box<Aa>;
        fn sub(lhs: &Aa, rhs: &Aa) -> Box<Aa>;
        fn neg(x: &Aa) -> Box<Aa>;
        fn constant(val: f64) -> Box<Aa>;

        fn clone_aa(val: &Box<Aa>) -> Box<Aa>;
    }
}

/*
use crate::{Ia, Aa, Symbols};

use std::sync::{Mutex, Arc};

*/
