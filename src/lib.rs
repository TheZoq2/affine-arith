pub mod ffi;
mod util;
// TODO: Re-enable for faster compile time
#[cfg(feature = "plot")]
pub mod plot;

use util::JoiningIterator;

/// Represents an Interval Arithmetic value
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Ia(f64, f64);

pub struct Symbols(usize);

impl Symbols {
    pub fn new() -> Self {
        Self(0)
    }

    pub fn new_symbol(&mut self) -> usize {
        let result = self.0;
        self.0 += 1;
        result
    }
}

/// Represents an Affine Arithmetic value
#[derive(Clone, PartialEq, Debug)]
pub struct Aa {
    pub center: f64,
    pub errors: Vec<(usize, f64)>,
}
impl Aa {
    pub fn from_bounds(min: f64, max: f64, symbols: &mut Symbols) -> Self {
        assert!(min <= max);
        let range = max - min;
        let center = min + range / 2.;

        Self {
            center,
            errors: vec![(symbols.new_symbol(), range / 2.)],
        }
    }

    pub fn constant(value: f64) -> Self {
        Self {
            center: value,
            errors: vec![],
        }
    }

    /// Computes the rad(\hat{x}) function described on page 48 of the paper.
    pub fn rad(&self) -> f64 {
        let Self { errors, .. } = self;
        errors.iter().map(|(_, magnitude)| magnitude.abs()).sum()
    }

    pub fn to_ia_range(&self) -> Ia {
        let Self { center, .. } = self;
        let r = self.rad();
        Ia(center - r, center + r)
    }

    /// Copmutes ax + by + c where where x and y are affine values. Delta is
    /// extra uncertainty to be added to the value and is assumed to be >= 0
    ///
    /// Section 3.6.3 for details
    pub fn general_affine(
        &self,
        rhs: &Aa,
        a: f64,
        b: f64,
        c: f64,
        delta: f64,
        symbols: &mut Symbols,
    ) -> Aa {
        let Aa {
            center: lc,
            errors: le,
        } = self;
        let Aa {
            center: rc,
            errors: re,
        } = rhs;

        let center = a * lc + b * rc + c;

        let mut errors = JoiningIterator::new(
            le.clone().into_iter(),
            re.clone().into_iter(),
            |(idx, x)| (idx, x * a),
            |(idx, x)| (idx, x * b),
            |(idx, lhs), (_, rhs)| (*idx, lhs + rhs),
            |(lhs, _), (rhs, _)| lhs.cmp(rhs),
        )
        .collect::<Vec<_>>();
        if delta != 0. {
            errors.push((symbols.new_symbol(), delta));
        }

        Aa { center, errors }
    }

    /// Copmutes the AA form of square root of the value. See stofi page 58 equations 3.8 to 3.14
    /// for details
    pub fn sqrt(&self, symbols: &mut Symbols) -> Self {
        // TODO: Special case if there are no uncertainty terms in the input
        let Ia(in_min, in_max) = self.to_ia_range();

        let min_sqrt = in_min.sqrt();
        let max_sqrt = in_max.sqrt();

        let alpha = 1. / (max_sqrt + min_sqrt);
        let c = (min_sqrt + max_sqrt) / 8.
            + 1. / 2. * (min_sqrt * max_sqrt) / (min_sqrt + max_sqrt);

        let delta = 1./8. * (max_sqrt - min_sqrt).powi(2) / (min_sqrt + max_sqrt);

        let mut errors = self.errors.iter()
            .map(|(i, x)| (*i, x*alpha))
            .collect::<Vec<_>>();
        errors.push((symbols.new_symbol(), delta));

        Self {
            center: alpha * self.center + c,
            errors
        }
    }
}

impl std::ops::Add for &Aa {
    type Output = Aa;
    fn add(self, rhs: Self) -> Self::Output {
        let Aa {
            center: lc,
            errors: le,
        } = self;
        let Aa {
            center: rc,
            errors: re,
        } = rhs;

        let center = lc + rc;

        let errors = JoiningIterator::new(
            le.clone().into_iter(),
            re.clone().into_iter(),
            |x| x,
            |x| x,
            |(idx, lhs), (_, rhs)| (*idx, lhs + rhs),
            |(lhs, _), (rhs, _)| lhs.cmp(rhs),
        )
        .collect();

        Aa { center, errors }
    }
}

impl std::ops::Sub for &Aa {
    type Output = Aa;
    fn sub(self, rhs: Self) -> Self::Output {
        self + &(-rhs)
    }
}

impl std::ops::Neg for &Aa {
    type Output = Aa;
    fn neg(self) -> Self::Output {
        let Aa { center, errors } = self;
        Aa {
            center: -center,
            errors: errors.iter().map(|(i, x)| (*i, -x)).collect(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_bounds_works() {
        let mut symbols = Symbols::new();
        let x = Aa::from_bounds(10., 15., &mut symbols);

        assert_eq!(
            x,
            Aa {
                center: 12.5,
                errors: vec![(0, 2.5)]
            }
        );
    }

    #[test]
    fn errors_of_non_similar_lengths_are_added() {
        {
            let mut symbols = Symbols::new();
            let x = Aa::from_bounds(10., 15., &mut symbols);
            let y = Aa::constant(5.);

            assert_eq!(
                &x + &y,
                Aa {
                    center: 17.5,
                    errors: vec![(0, 2.5)]
                }
            );
        }
        {
            let mut symbols = Symbols::new();
            let y = Aa::from_bounds(10., 15., &mut symbols);
            let x = Aa::constant(5.);

            assert_eq!(
                &x + &y,
                Aa {
                    center: 17.5,
                    errors: vec![(0, 2.5)]
                }
            );
        }
    }

    #[test]
    fn x_plus_neg_x_has_range_0() {
        let x = Aa {
            center: 10.,
            errors: vec![(0, 2.5), (1, 3.5)],
        };

        assert_eq!((&x + &(-&x)).to_ia_range(), Ia(0., 0.));
    }

    #[test]
    fn general_affine_works() {
        {
            let mut symbols = Symbols::new();

            let x = Aa::from_bounds(-5., 15., &mut symbols);
            let y = Aa::constant(7.);

            let result = x.general_affine(&y, 2., 3., 1., 1., &mut symbols);

            assert_eq!(
                result,
                Aa {
                    center: 10. + 7. * 3. + 1.,
                    errors: vec![(0, 20.), (1, 1.0)]
                }
            );
        }
    }


    #[test]
    fn sqrt() {
        fn check_with_bounds(min: f64, max: f64) {
            let mut symbols = Symbols::new();
            let input = Aa::from_bounds(min, max, &mut symbols);

            let output = input.sqrt(&mut symbols);

            // Ensure the error array is as long as we expect
            assert_eq!(output.errors.len(), 2);

            // Get the components of the value
            let center = output.center;
            let (_, slope) = output.errors[0];
            let (_, error) = output.errors[1];

            // Check 100 points along the line
            for xi in 0..=100 {
                let x = min + xi as f64 / 100. * (max-min);
                // The error variable which should be between -1 and 1
                let eps = xi as f64 / 50. - 1.;

                let computed = center + eps * slope;
                let actual = x.sqrt();
                let diff = (computed - actual).abs();
                assert!(
                    diff - f64::EPSILON <= error,
                    "Error too large at x: {}, eps: {}, computed: {}, actual: {}, error: {}, diff: {}",
                    x,
                    eps,
                    computed,
                    actual,
                    error,
                    diff
                );
            }

            // Ensure that the error is spot on at the bounds
            assert!((min.sqrt() - (center - slope - error)).abs() <= f64::EPSILON);
            assert!((max.sqrt() - (center + slope - error)).abs() <= f64::EPSILON);
        }

        check_with_bounds(1., 2.);
        check_with_bounds(0., 2.);
        check_with_bounds(100., 150.);
    }
}
